const express = require('express');
const {db} = require('./database/db');
const authRoutes = require('./routes/authRoutes');
const usersRoutes = require('./routes/userRoutes');
const productsRoutes = require('./routes/productRoutes');
const ordersRoutes = require('./routes/orderRoutes');

// const cors = require("cors");

// log db events to console
db.on('error', console.error.bind(console, 'DB Connection Error.'));
db.once('open', () =>
  console.log(`${new Date().toLocaleTimeString()}| Connected to MongoDB`),
);

// init express
const app = express();
const port = process.env.PORT || 8000;
//app.use(cors());


// express routes
app.use('/auth', authRoutes);
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);
app.use('/orders', ordersRoutes);

app.get('/', (req, res) =>
  res.send({
    status: 'Running',
    version,
    endpoints: listEndpoints(app)
      .flatMap((entry) =>
        entry.methods.map((method) => {
          return `[${method}] ${req.protocol}://${req.hostname}:${port}${entry.path}`;
        }),
      )
      .sort(),
  }),
);

// start express
app.listen(port, () =>
  console.log(`${new Date().toLocaleTimeString()}| Server is running on ${port}`),
);
