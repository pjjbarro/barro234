const procfile = require('../procfile');
const user = require('../models/user');
const bycrypt = require('bcrypt');
const {withValidateBody} = require('../middleware/validator');
const {mongoErrorToStatus, mongoErrorToMessage} = require('../database/db');
const {createAccessToken} = require('../middleware/auth');

//Creates a new user with fields email, password, firstName, lastName, mobileNo.

module.exports.createUser = withValidateBody([
  'email',
  'password',
  'firstName',
  'lastName',
  'mobileNo',
])((req, res) => {
  const {email, password, firstName, lastName, mobileNo} = req.body;

  if (password.length < 8) {
    return res.status(422).send({message: constants.messages.PASSWORD_TOO_SHORT});
  }

  const doc = new User({
    email,
    password: bycrypt.hashSync(password, 10),
    firstName,
    lastName,
    mobileNo,
  });
  doc
    .save()
    .then((savedDoc) =>
      res.status(201).send({message: constants.messages.USER_CREATED, id: savedDoc._id}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
});


 //Logs the user in, returns a JWT to client if login is successful

module.exports.loginUser = (req, res) => {
  	const {email, password} = req.body;
  	if (!email || !password) {
    return res
      .status(401)
      .send({message: constants.messages.EMAIL_AND_PASSWORD_ARE_REQUIRED});
  }
  User.findOne({email})
    .then((result) => {
      if (result === null) {
        return res.status(401).send({message: constants.messages.COULD_NOT_FIND_USER});
      }
      const isPasswordValid = bycrypt.compareSync(password, result.password);
      if (isPasswordValid) {
      	  return res.send(createAccessToken(result));
      } else {
       	 return res.status(401).send({message: constants.messages.PASSWORD_IS_INCORRECT});
      }
    })
    	.catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


 //Gets all users. Passwords fields are skipped on return
 
module.exports.getAllUsers = (req, res) => {
  User.find({}, {password: 0})
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


 //Gets a single user by id (from URL params).
 
module.exports.getUserById = (req, res) => {
 	 User.findById(req.params.id, {password: 0})
    .then((result) =>
      result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_USER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


//Gets the profile of user currently authenticated.
module.exports.getUserProfile = (req, res) => {
  User.findById(req.user.id, {password: 0})
    	.then((result) =>
     	 result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_USER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


 //Updates the profile of user authenticated
 
module.exports.updateUserProfile = (req, res) => {
 	 const {firstName, lastName, mobileNo} = req;
  	const updates = {
   		 firstName,
    	lastName,
    	mobileNo,
  };
 User.findByIdAndUpdate(req.user.id, updates, {new: true})
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

 //Sets isAdmin to true
 
module.exports.addAdmin = (req, res) => {
 	 	const updates = {
    	isAdmin: true,
  };
  User.findByIdAndUpdate(req.params.id, updates, {new: true})
   		 .then((result) =>
     	 res.send({
        message: `${result.email} ${constants.messages.ADDED_TO_ADMIN}`,
      }),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


 // Sets isAdmin to false

module.exports.removeAdmin = (req, res) => {
 		 const updates = {
   		 isAdmin: false,
  };
  User.findByIdAndUpdate(req.params.id, updates, {new: true})
    	.then((result) =>
     
     	 res.send({
        message: `${result.email} ${constants.messages.REMOVED_FROM_ADMIN}`,
      }),
    )
    	.catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};
