const procfile = require('../procfile');
const order = require('../models/order');
const product = require('../models/product');
const {mongoErrorToStatus, mongoErrorToMessage} = require('../database/db');


//Creates a new order from products
 
module.exports.createOrder = async (req, res) => {
  // only regular users can create order
  if (req.user.isAdmin) {
    return res.status(403).send({
      message: constants.messages.FORBIDDEN_ONLY_REGULAR_USERS,
    });
  }

  // products to be in {id: <id>, quantity: <quantity>} format
  const {products} = req.body;
  if (!products || !Array.isArray(products) || products.length < 1) {
    return res.status(422).send({
      message: constants.messages.LIST_OF_PRODUCTS_IS_REQUIRED,
    });
  } else {
    // validate product items
    const hasValidationErrors = products.find((product) => {
      if (!product.id || !product.quantity) {
        return true;
      }
    });
    if (hasValidationErrors) {
      return res.status(422).send({
        message: constants.messages.ORDER_ITEMS_MUST_HAVE,
      });
    }
  }

  // Retrieve products, to get name and price detail.
  let productDocs = [];
  try {
    const ids = products.map((product) => product.id);
    productDocs = await Product.find({_id: {$in: ids}, isActive: true});
  } catch (e) {
    return res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e));
  }

  const orderItems = products.map((product) => {
    const actualProduct = productDocs.find((p) => p.id === product.id);
    return {
      productId: product.id,
      name: actualProduct.name,
      price: actualProduct.price,
      quantity: product.quantity,
    };
  });

  const totalAmount = orderItems.reduce((accumulatedTotal, currentOrderItem) => {
    return accumulatedTotal + currentOrderItem.price * currentOrderItem.quantity;
  }, 0);

  const clientTotalAmount = req.body.totalAmount;
  if (clientTotalAmount && clientTotalAmount !== totalAmount) {
    return res.status(422).send({
      message: constants.messages.CLIENT_TOTAL_AMOUNT_MISMATCH,
      details: {
        received: clientTotalAmount,
        expected: totalAmount,
      },
    });
  }

  console.log('$$$ Total (-: ', totalAmount);
  const doc = new Order({
    userId: req.user.id,
    totalAmount,
    products: orderItems,
  });
  doc
    .save()
    .then((savedDoc) => res.status(201).send(savedDoc))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


//Gets all orders. Admin gets all system order, user gets only their order

module.exports.getAllOrders = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin ? {} : {userId: req.user.id};
  Order.find(criteria)
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


// Must be Admin or owner of order
 
module.exports.getOrderById = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin
    ? {_id: req.params.id}
    : {_id: req.params.id, userId: req.user.id};
  Order.findOne(criteria)
    .then((result) =>
      result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_ORDER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};


//Must be Admin

module.exports.updateOrderStatusById = (req, res) => {
  Order.findOne({_id: req.params.id})
    .then((order) => {
      order.status = req.body.status;
      return order.save();
    })
    .then((result) =>
      result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_ORDER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};
