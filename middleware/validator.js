const constants = require('../procfile');

module.exports.validateBodyMiddleware = (requiredFields) => (req, res, next) => {
  	if (Array.isArray(requiredFields)) {
    const errors = [];
    requiredFields.forEach((field) => {
      if (!req.body[field]) {
        errors.push(`${field} is required`);
      }
    });
    if (errors.length > 0) {
      return res.status(422).send({
        message: constants.messages.MISSING_REQUIRED_FIELDS,
        errors,
});
    }
    next();
 	 } else {
   	 console.warn(
      'Data passed to validateBodyFields was not array. No validation was done!',
    );
    next();
  }
};

module.exports.withValidateBody = (requiredFields) => (callback) => (req, res, next) => {
 	 const errors = [];
 	 requiredFields.forEach((field) => {
    if (!req.body[field]) {
      errors.push(`${field} is required`);
    }
 });
 	 if (errors.length > 0) {
   	 return res.status(422).send({
      message: constants.messages.MISSING_REQUIRED_FIELDS,
      errors,
});
  	}
  	return callback(req, res, next);
};
