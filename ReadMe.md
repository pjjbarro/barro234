Features
User registration
User authentication
Set user as admin (Admin only)
Create Product
Retrieve All Active Products
Retrieve All Products (Admin only)
Update Product
Archive Product (Admin only)
Search Products
Get Products By Category
Create Order
Update Order Status (Admin only)
Get authenticated user’s orders
Get All Orders
Registration
POST - http://localhost:8000/users
Body (JSON):
{
“firstName”: String,
“lastName”: String,
“email”: String,
“password”: String,
“mobileNo”: String,
}

Login
POST - http://localhost:8000/auth
Body (JSON):
{
“email”: String,
“password”: String
}

Set As Admin
PUT - http://localhost:8000/users/addAdmin/{id}
Admin Token Required

Set as regular user
PUT - http://localhost:8000/users/removeAdmin/{id}
Admin Token Required

Create Product
POST - http://localhost:8000/products
Body (JSON):
{
“name”: String,
“description”: String,
“price”: Number,
“images”?: String[],
“category”?: String,
}

Retrieve All Active Products
GET - http://localhost:8000/products
Apply Filters:
GET - http://localhost:8000/products?{filter}={value}
Supported filters: minPrice, maxPrice, category

Retrieve All Products (including inactive)
GET - http://localhost:8000/products/all
Admin Token Required

Retrieve Single Product
GET - http://localhost:8000/product/{id}

Update Product
PUT - http://localhost:8000/products
Body (JSON):
{
“name”: String,
“description”: String,
“price”: Number,
“images”?: String[],
“category”?: String,
}
Admin Token Required

Archive Product
PUT - http://localhost:8000/products/archive/{id}
Admin Token Required

Activate Product
PUT - http://localhost:8000/products/activate/{id}
Admin Token Required

Search Products
GET - http://localhost:8000/products/search/{text}
Apply Filters:
GET - http://localhost:8000/products/search/{text}?filter={value}
Supported filters: minPrice, maxPrice, category

Get Products in Category
GET - http://localhost:8000/products/category/{category}

Create Order
POST - http://localhost:8000/orders
Body (JSON):
{
totalAmount?: Number,
products: [
{
id: String,
quantity: Number
},
{
id: String,
quantity: Number
},
{
id: String,
quantity: Number
}
…
]
}

Update Order Status
PUT - http://localhost:8000/orders/setStatus/{id}
Body (JSON):
{
status: String
} Must be processing, shipping, completed or cancelled
Admin Token Required

Retrieve Authenticated User’s orders
GET - http://localhost:8000/orders

Retrieve All Orders
GET - http://localhost:8000/orders
Admin Token Required