const express = require('express');
const router = express.Router();
const {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrderStatusById,
} = require('../controllers/orderController');

const {verifyAuth, verifyAdmin} = require('../middleware/auth');

router.post('/', verifyAuth, createOrder);

router.get('/', verifyAuth, getAllOrders);

router.get('/:id', verifyAuth, getOrderById);

router.put('/setStatus/:id', verifyAuth, verifyAdmin, updateOrderStatusById);

module.exports = router;
