const express = require('express');
const router = express.Router();
const {
  createProduct,
  getAllProducts,
  getAllActiveProducts,
  getProductById,
  updateProductById,
  archiveProductById,
  activateProductById,
  searchProducts,
  getCategory,
} = require('../controllers/productController');

const {verifyAuth, verifyAdmin, attachUserIfPresent} = require('../middleware/auth');

router.post('/', verifyAuth, verifyAdmin, createProduct);

router.get('/', getAllActiveProducts);

router.get('/all', verifyAuth, verifyAdmin, getAllProducts);

router.get('/:id', getProductById);

router.put('/:id', verifyAuth, verifyAdmin, updateProductById);

router.put('/archive/:id', verifyAuth, verifyAdmin, archiveProductById);

router.put('/activate/:id', verifyAuth, verifyAdmin, activateProductById);

router.get('/search/:text', attachUserIfPresent, searchProducts);

router.get('/category/:categoryName', attachUserIfPresent, getCategory);

module.exports = router;
