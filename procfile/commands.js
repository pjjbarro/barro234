module.exports.AUTH_FAIL_NO_TOKEN = 'Authentication Failed. No Token';
module.exports.AUTH_FAIL_INVALID_TOKEN = 'Authentication Failed. Invalid/expired token';
module.exports.AUTH_FAIL_NO_PERMISSION = "Forbidden. You don't have required permissions";
module.exports.PASSWORD_TOO_SHORT = 'Password is too short';
module.exports.PASSWORD_IS_INCORRECT = 'Username/Password incorrect';
module.exports.COULD_NOT_FIND_USER = 'Could not find user';
module.exports.EMAIL_AND_PASSWORD_ARE_REQUIRED = 'Email and Password fields are required';
module.exports.MISSING_REQUIRED_FIELDS = 'There are required fields missing';
module.exports.USER_CREATED = 'User created';
module.exports.AN_ERROR_OCCURED = 'An error occured';
module.exports.NOT_FOUND = 'Not found';
module.exports.INVALID_REQUEST_ARGUMENTS = 'Invalid request or missing necessary info';
module.exports.COULD_NOT_FIND_PRODUCT = 'Could not find product';
module.exports.ARCHIVED = 'archived';
module.exports.ACTIVATED = 'activated';
module.exports.ADDED_TO_ADMIN = 'added to admins';
module.exports.REMOVED_FROM_ADMIN = 'removed from admins';
module.exports.PRODUCT_CREATED = 'Product created';
module.exports.NO_PRODUCTS_FOUND = '0 products found';
module.exports.ORDER_CREATED = 'Order created';
module.exports.LIST_OF_PRODUCTS_IS_REQUIRED = 'Order with at least one product required';
module.exports.FORBIDDEN_ONLY_REGULAR_USERS =
  'Forbidden. Only available for regular users';
module.exports.ORDER_ITEMS_MUST_HAVE = 'All order items have id and quantity';
module.exports.INVALID_ORDER_REQUEST_ARGUMENTS =
  'Invalid order data or missing necessary info';
module.exports.CLIENT_TOTAL_AMOUNT_MISMATCH =
  'The provided total amount does not match the expected total';
module.exports.IMAGES_MUST_BE_AN_ARRAY = 'Images must be an array';
module.exports.ORDER_STATUS_UPDATED = 'Order status has been updated';
